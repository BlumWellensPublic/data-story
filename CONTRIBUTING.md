# Contributing to Data Story Specification

We welcome contributions to the Data Story Specification! Whether it's submitting bug reports, proposing new components, or improving existing ones, your help is appreciated.

## How to Contribute

1. Fork the repository.
2. Create a feature branch: \`git checkout -b feature/YourFeatureName\`
3. Commit your changes: \`git commit -am 'Add some feature'\`
4. Push to the branch: \`git push origin feature/YourFeatureName\`
5. Submit a pull request.

## Contribution Guidelines

- Ensure your contributions are well-documented.
- Follow the existing structure and formatting.
- Discuss major changes in an issue before committing.

Thank you for your contributions!