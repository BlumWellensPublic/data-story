Data Story Specification v0.7.0
-------------------------------
# Contents
- Introduction
- Language Syntax and Constructs
    - Elements and Structure
        - Basic Syntax
        - Syntax for Inheritance and Composition
            - Syntax for Inheritance
                - Example 01
            - Syntax for Composition
                - Example 02
            - Additional Considerations
                - File Path Resolution
                - Overriding and Merging Rules
    - Core Constructs:
        - Pattern, Case, Scenario, Story \(PCS\):
            - Definition
            - Structure:
            - Elements
                - Type
                - Version
                - Attribution
                - Status
                - Description \(DESC\):
                - Trade-offs \(TO\):
                - Actors and Systems \(AS\)
                - Flow Control \(FC\)
                - Integration Points \(IP\)
                - Data Schema \(DS\)
                - Security, Privacy, and Compliance
                    - Security and Privacy \(SP\)
                    - Compliance and Regulatory \(CR\)
                    - User and Role Management \(URM\)
                - Operational Excellence and Performance Constructs \(QoS, ML, NA\):
                    - Quality of Service \(QoS\)
                    - Monitoring and Logging \(ML\)
                    - Notification and Alerting \(NA\)
                - Versioning and Evolution \(VE\):
                - Error Handling and Recovery \(EHR\)
                - Data Lifecycle Management \(DLM\)
                - Data Governance \(DG\)
                - Documentation, Metadata, and Data Provenance
                    - Documentation and Metadata \(DM\)
                    - Data Lineage \(DL\)
                    - Data Provenance \(DM-DP\)
                - Interoperability Standards \(IS\)
                - Data Transformation and Mapping \(DTM\)
- Example Data Stories
    - Example 03
    - Example 04
    - Example 05
    - Example 06
    - Example 07


# Introduction

Data Story is a domain-specific language designed to articulate the requirements, operations, and governance frameworks for Enterprise Data Platforms (EDP). It facilitates the precise communication of technical specifications and business requirements among stakeholders involved in the design, implementation, and management of EDPs.

# Language Syntax and Constructs

## Elements and Structure
Specifications in Data Story are constructed from predefined elements. Each element represents a different aspect of the EDP specification, such as data workflows, security protocols, and performance metrics.

### Basic Syntax
The language is structured around key-value pairs, with values ranging from simple strings to complex nested elements and lists. The following rules guide the writing of a Data Story

1. Naming Elements

    General Rule: Element names in Data Story should be concise, descriptive, and reflect the element's purpose or function within the EDP specification.
    Format: Use CamelCase for multi-word names. Start each element with a capital letter, and do not include spaces between words. Example: DataIngestion, ErrorHandling.
    Special Characters: Avoid using special characters or symbols in names, with the exception of underscores (_) to denote private or internal elements.

2. Handling Whitespace

    Flexibility: Data Story is designed to be readable and therefore is flexible with the use of whitespace (spaces, tabs, and new lines).
    Best Practice: Use indentation to denote nested elements or constructs. A common practice is to use two or four spaces for each level of indentation, but tabs are also acceptable.
    New Lines: Use new lines to separate elements or constructs for better readability. For example, start each key-value pair or nested element on a new line.

3. Case Sensitivity

    Keywords: The Data Story DSL is case-sensitive. Keywords and constructs such as PCS, @Inherit, and @Compose must be written in the exact case as specified in the documentation.
    Element Names: Similarly, names given to elements, including types, attributes, and identifiers, are case-sensitive. Consistency in case usage is crucial for avoiding errors and ensuring the correct interpretation of the specification.

4. Comments

    Purpose: Comments are used to add explanatory notes or provide additional information that is not part of the executable specification.
    Syntax: Use double slashes (//) to start a single-line comment. For multi-line comments, enclose the text within /* and */.
    Example:

    plaintext

    // This is a single-line comment
    /*
      This is a multi-line comment
      which spans over multiple lines
    */

5. Lists and Nested Elements

    Lists: Define lists using brackets []. Lists can contain strings, numbers, or nested elements. Items in a list are separated by commas.
    Nested Elements: Use curly braces {} to enclose nested elements. This allows for hierarchical structuring within the specification.
    Example:

    plaintext

    DataSources: [
      "SensorData",
      "LogFiles",
      {
        Name: "ExternalAPIs",
        Type: "RESTful"
      }
    ]

6. Strings and Literals

    Quotation Marks: Enclose strings in double quotation marks " ".
    Numeric Literals: Write numbers without quotation marks. Decimal points are allowed for floating-point numbers.
    Boolean Literals: Use true or false without quotation marks for boolean values.

7. Key-Value Pairs

    Syntax: Separate keys and values with a colon :. Follow the colon with a space for readability.
    Example: Version: "1.0.0"

8. Units Specification

    Specifying units is essential for accurately defining performance metrics, resource requirements, and operational thresholds in data platform specifications. Explicit unit designation helps prevent misunderstandings and ensures clear communication across diverse stakeholder groups.

    #### Syntax for Specifying Units

        General Format: {value} [unit]
        Where:
            {value} is a numerical value (integer or floating-point).
            [unit] is the abbreviation of the unit in which the value is measured.

    #### Supported Units

        Time: Specified in seconds (s), milliseconds (ms), hours (h), or minutes (m). Example: 10 [s], 200 [ms], 1 [h], 30 [m].
        Data Size: Specified in bytes (B), kilobytes (KB), megabytes (MB), gigabytes (GB), or terabytes (TB). Example: 500 [MB], 2 [GB], 100 [KB].
        Frequency: Specified in hertz (Hz), representing the number of events per second. Example: 60 [Hz].

    #### Data Types Specification

    Specifying data types enhances the clarity of data schema definitions, enabling stakeholders to understand the kinds of data being handled, stored, and processed within the EDP. It provides a foundation for ensuring data integrity and type safety.
    Syntax for Specifying Data Types

        General Format: "key": "[data type]"
        Where:
            "key" is the name of the data element.
            [data type] is the specified type of the data element.

    #### Supported Data Types

        String: A sequence of characters, enclosed in quotes. Example: "name": "string".
        Integer: A whole number, without a fractional part. Example: "age": "integer".
        Float: A number with a fractional part, represented in decimal or exponential notation. Example: "temperature": "float".
        Boolean: A true or false value. Example: "isActive": "boolean".
        Datetime: A specific date and time, conforming to a recognized format (e.g., ISO 8601). Example: "createdAt": "datetime".

    #### Example: Enhanced Data Story Specification

    A Data Story specification incorporating explicit units and data types:

    DataStory

    PCS: "Optimized Data Transfer Protocol" {
      Version: "1.0.1",
      DESC: "Defines a protocol for efficient data transfer within the EDP, focusing on minimizing latency and maximizing throughput.",
      QoS: {
        "Max Latency": "2 [s]",
        "Min Throughput": "500 [MB/s]"
      },
      DS: {
        "fileId": "string",
        "fileSize": "integer [MB]",
        "transferRate": "float [MB/s]",
        "transferComplete": "boolean",
        "timestamp": "datetime"
      }
    }

    In this example, the specification clearly outlines quality of service (QoS) expectations with explicit units and defines the data schema with specific data types, including units where applicable. This approach not only aids in precise communication but also supports the formulation of specifications that are readily understandable by all stakeholders involved in the EDP's lifecycle.    

### Syntax for Inheritance and Composition

The following syntax enhancements facilitate modularity and reusability in the Data Story DSL, particularly for supporting inheritance and composition. These enhancements allow components to inherit attributes from other components and to compose more complex constructs from simpler, reusable pieces.

#### Syntax for Inheritance

Inheritance allows a Data Story component to extend another, inheriting its properties and potentially overriding or adding new ones. This is particularly useful for creating specific versions of patterns, scenarios, or any PCS constructs that share common features.

Structure: `@Inherit: <path_to_parent_component>`

##### Example 01
PCS: "Enhanced Data Ingestion Scenario" {
  @Inherit: "/patterns/data_ingestion.md",
  Version: "1.1.0",
  DESC: "This scenario extends the standard data ingestion pattern with enhancements for error handling and data validation.",
  EHR: "Custom error handling logic specific to this scenario."
}

In this syntax example, @Inherit points to the path of the parent component. The inheriting component can then specify additional attributes or override inherited ones where necessary.

#### Syntax for Composition

Composition involves embedding one or more components within another. This enables building complex constructs out of simpler, modular pieces, facilitating code reuse and maintainability.

Structure: 
```
@Compose: [
  { Path: "<path_to_component>", Alias: "<optional_alias>" },
  ...
]
```

##### Example 02
PCS: "Comprehensive Data Processing Workflow" {
  @Compose: [
    { Path: "/scenarios/real_time_processing.md", Alias: "RealTime" },
    { Path: "/scenarios/batch_processing.md", Alias: "Batch" }
  ],
  DESC: "This workflow combines real-time and batch processing to handle diverse data loads efficiently.",
  AS: ["Data Engineer", "Processing System", "RealTime.AS", "Batch.AS"]
}


In the composition syntax example, @Compose includes an array of components to embed, each with a Path to the component file and an optional Alias for referencing within the parent component. Components are referenced by their aliases for clarity and to avoid namespace collisions.

#### Additional Considerations

##### File Path Resolution
The paths to parent or composed components should be relative to a predefined root in the repository or an absolute path within the repository structure. This requires establishing a clear directory structure and naming conventions for ease of reference.

##### Overriding and Merging Rules
Clearly define how properties from inherited or composed components are merged or overridden. For instance, lists might be concatenated, while singular values or objects might be overridden by the child component's definitions.

## Core Constructs:

### Pattern, Case, Scenario, Story (PCS):

#### Definition
The PCS construct serves as the foundational element of the DataSpec DSL, encapsulating detailed specifications of data management patterns, real-world cases, specific scenarios, or user stories relevant to the design and operation of Enterprise Data Platforms (EDP).

#### Structure:
```DataStory
PCS: "Name" {
  Type: "Pattern" | "Case" | "Scenario" | "Story",
  Version: "x.y.z",
  Attribution: {
    "Name": "Author Name",
    "Affiliation": "Author Affiliation",
    "Contact": "author.email@example.com"
  },
  Status: "Draft" | "Approved" | "Deprecated",
  DESC: "Detailed description of the PCS, outlining the context, goals, expected impact, and requirements.",
  TO: "Specific trade-offs, such as between speed and accuracy, highlighting preferences.",
  AS: ["List of actors and systems involved, distinguishing between human and system actors."],
    ... (Optional elements FC, IP, DS, etc., as required from Elements list below)
}
```

#### Elements

##### Type
Type: "Pattern" | "Case" | "Scenario" | "Story"
	Purpose: Specifies the category of the PCS, enhancing clarity and facilitating appropriate application in EDP specifications.
	- Pattern:
		A Pattern is a reusable solution to a commonly occurring problem within a given context in data platform design and operations. Patterns capture best practices, offering a standardized approach to solving specific types of challenges. They are abstract enough to be applied across different situations but detailed enough to provide actionable guidance.
	        Example: See ./patterns/README.md for some example patterns. In general, that folder contains standard EDP patterns.

	- Case:
		A Case is a detailed account of a specific instance where certain data platform designs, technologies, or methodologies were applied to address particular business or technical challenges. Cases are real-world examples that provide insights into the practical application of data platform solutions, including successes, challenges, and lessons learned.
	        Example: See ./use_cases/README.md for a use case to import data into the EDP from a CRM. In general, that folder contains standard EDP use cases.

	- Scenario:
		A Scenario describes a specific sequence of actions and events within the context of a data platform. Scenarios are used to illustrate how various components of a data platform interact in response to certain triggers or conditions, often focusing on operational workflows, data processing pipelines, or user interactions.
	        Example: See ./scenarios/README.md for example scenarios. In general, that folder contains standard EDP scenarios.


	- Story:
		A Story, often referred to as a "user story," is a short, simple description of a feature or requirement from the perspective of an end user or stakeholder of a data platform. Stories emphasize the value or benefit of a feature or functionality to the user, focusing on what they need and why.
	        Example: 
          Example: See ./user_stories/README.md for example user stories. In general, that folder contains standard EDP stories.

##### Version
Version: "x.y.z"
Purpose: Adheres to [semantic versioning](https://semver.org/), allowing for precise tracking of revisions and updates to the PCS.
Guidelines:
    **Major Version Update**: Changes that make backward-incompatible amendments to the PCS construct, such as removing or renaming keys, changing the type of values expected for a specific key, or modifying the PCS structure in a way that would require changes in how PCS instances are interpreted or processed.
    **Minor Version Update**: Additions that are backward-compatible, such as adding new optional keys to the PCS construct, enhancing the PCS description with additional details without changing the meaning of existing keys, or introducing new features in the PCS construct that do not affect existing PCS instances.
    **Patch Version Update**: Backward-compatible bug fixes or corrections that do not change the structure of the PCS construct but may correct spelling mistakes, improve clarity in the description of PCS instances, or fix errors in examples.

##### Attribution
Attribution: {...}
Purpose: Acknowledges the author(s) or contributor(s), supporting collaboration and recognition of contributions.

##### Status
Status: "Draft" | "Approved" | "Deprecated",
Purpose: Indicates the readiness and review state of the PCS, guiding its use in project planning and implementation.

##### Description (DESC):
DESC: "Text" 
Purpose: Provides a detailed explanation of the PCS, including objectives, expected decisions, expected outcomes.

##### Trade-offs (TO):
TO: "Text" 
Purpose: Outlines any specific trade-offs between competing requirements, such as speed versus accuracy.
Example:
	TO: "Prefers accuracy over speed."

##### Actors and Systems (AS)
AS: ["Actor1", "System1"] 
Purpose: Lists the human and system actors involved in the scenario.

##### Flow Control (FC)
FC: {...} 
Purpose: Introduces logic control structures like conditional branches, loops, and event triggers.
Example:
	FC: {
	  IF: "(condition)" { "action" },
	  LOOP: "(condition)" { "action" },
	  ON: "event" { "action" }
	}

##### Integration Points (IP)
IP: {...} 
Purpose: Specifies the details of interactions with external systems, including protocols, data formats, and communication standards. It describes how the EDP interfaces with other systems, APIs, or data sources.

##### Data Schema (DS)
DS: {...} 
Purpose: Defines the data structure involved in the PCS, supporting complex types and validation.
Examples:
	Example 01
		DS: {
		  "deviceId": "string",
		  "timestamp": "datetime",
		  "value": "float"
		}
	Example 02
		DS: {
	      "customerId": "string",
	      "purchaseAmount": "float",
	      "purchaseDate": "datetime"
    }

##### Security, Privacy, and Compliance
This section addresses the critical aspects of ensuring data security, maintaining privacy, and adhering to compliance and regulatory standards within the Enterprise Data Platform (EDP). It underscores the interdependencies of these areas and provides a comprehensive framework for safeguarding data and operations against breaches, unauthorized access, and non-compliance penalties.

###### Security and Privacy (SP)
SP: {...} 
Purpose: Detail the measures and technologies implemented to protect data against unauthorized access, breaches, and leaks. This includes encryption, access controls, and data masking techniques that safeguard data at rest and in transit.
Implementation Guidelines:
  All data, whether at rest or in transit, must be encrypted using industry-standard encryption protocols.
  Implement strict access control policies, ensuring that individuals have access only to the data necessary for their role.
  Regularly update and patch security systems to protect against known vulnerabilities.

###### Compliance and Regulatory (CR)
CR: {...} 
Purpose: Outline the compliance and regulatory frameworks that the EDP must adhere to, including data protection laws (such as GDPR or HIPAA), industry standards, and internal governance policies. It guides how the system is designed and operated to meet these requirements.
Implementation Guidelines:
  Maintain a comprehensive list of all applicable regulatory and compliance standards.
  Implement mechanisms for regular compliance audits and reviews to ensure ongoing adherence.
  Design data handling and processing workflows to automatically enforce compliance rules, reducing the risk of non-compliance.
Focus: CR primarily deals with external requirements imposed on the organization. This includes laws like GDPR for data protection, HIPAA for health information privacy, industry standards, and any regulatory mandates specific to the business sector.
Application: CR is reactive to the external environment and requires updates and adjustments based on changes in laws and regulations.  

###### User and Role Management (URM)
URM: {...} 
Purpose: Define the framework for managing user access and roles within the EDP, including authentication mechanisms, role-based access control (RBAC), and permissions management. This construct ensures that users can access or modify data and system functionalities strictly according to their roles and responsibilities.
Implementation Guidelines:
  Implement a robust RBAC system to manage user roles and permissions, ensuring that access to data and functions aligns with user responsibilities.
  Use multi-factor authentication (MFA) and strong password policies to enhance user account security.
  Regularly review and adjust user roles and permissions to reflect changes in responsibilities or employment status.
Application: Central to implementing effective security and access control within the platform.

##### Operational Excellence and Performance Constructs (QoS, ML, NA):
Quality of Service (QoS), Monitoring and Logging (ML), and Notification and Alerting (NA) all contribute to the operational excellence and performance monitoring of the EDP.

###### Quality of Service (QoS)
QoS: {...} 
Purpose: Parameters that define the expected performance, reliability, and availability of the data platform and its services. This could include aspects like response times, throughput, data refresh rates, and uptime guarantees.
Application: Helps in setting expectations and designing systems that meet operational and user experience standards.

###### Monitoring and Logging (ML)
ML: {
  "Operational Metrics": {
    "Description": "Details the system performance, user activities, and security events to be monitored. Specifies the metrics for system health, including CPU usage, memory consumption, and response times.",
    "Logging Format": "Defines the structured format for logging operational metrics, ensuring consistency and readability.",
    "Retention Policy": "Outlines how long logs are retained, balancing between auditability and storage considerations."
  },
  "Data Quality Monitoring": {
    "Description": "Specifies the framework for continuously monitoring data quality, leveraging tools and libraries like Great Expectations or AWS' Deequ to define and enforce data quality rules.",
    "Quality Metrics": {
      "Completeness": "Percentage of non-null values in a dataset.",
      "Uniqueness": "Checks for duplicates, ensuring each data entry is unique.",
      "Consistency": "Ensures data across different sources or datasets follows the same format and units.",
      "Accuracy": "Verifies data against a trusted source or set of rules for correctness.",
      "Timeliness": "Ensures data is up-to-date and available when needed."
    },
    "Rules Management": {
      "Definition": "Leveraging external libraries to define data quality rules. For instance, Great Expectations allows for declarative specifications of data expectations, while AWS' Deequ supports scalable data testing.",
      "Enforcement": "Mechanisms to automatically apply these rules on incoming data streams or at specified intervals, flagging data that does not conform.",
      "Notification": "Automated alerts to data engineers or stewards when data quality issues are detected, facilitating prompt action."
    },
    "Integration with Data Quality Assurance": "This component works in tandem with the 'Data Quality Assurance Framework' to not only flag issues but also provide metrics and logs that inform continuous improvement of data quality strategies."
  },
  "Metadata Management": {
    "Data Cataloging": "Logging the metadata of incoming data streams, including source, schema, and version, to aid in data discovery and governance.",
    "Change Tracking": "Maintaining logs of schema or data changes over time, supporting audit trails and impact analysis."
  },
  "Compliance and Audit Logs": {
    "Description": "Details the recording of access logs, data modifications, and system changes to comply with regulatory requirements. Includes who accessed what data, when, and what actions were performed.",
    "Retention": "Specifies the retention period for compliance logs, in alignment with legal and regulatory standards."
  }
} 
Purpose: Specifies which aspects of the EDP should be monitored and logged, including system performance, data quality metrics, user activities, and security events. It also outlines the logging format and retention policies.
Application: Essential for operational oversight, security, and compliance auditing.

###### Notification and Alerting (NA)
NA: {...} 
Purpose: Sets up mechanisms for notifying users or systems about important events, changes, or errors within the EDP. It includes configurations for alert thresholds, delivery channels, and escalation paths.
Application: Improves responsiveness and enables proactive management of the data platform.

##### Versioning and Evolution (VE):
VE: {...} 
Purpose: Mechanisms for managing changes to data schemas, APIs, and processes over time. This includes version control strategies and migration pathways for both data and platform architecture.
Application: Ensures that the data platform can evolve without disrupting existing operations or data integrity.

##### Error Handling and Recovery (EHR)
EHR: {...} 
Purpose: Details the strategies for detecting, logging, and recovering from errors or exceptions in the EDP. It covers both system-level and data-level error handling mechanisms. Specifies how the system detects, logs, and recovers from errors or anomalies in data processing and management workflows.
Application: Enhances system robustness and reliability, ensuring continuity of operations under adverse conditions.

##### Data Lifecycle Management (DLM)
DLM: {
  "Creation": {
    "Description": "Defines how data is captured or generated within the system, including initial data acquisition methods."
  },
  "Storage": {
    "Description": "Details the storage solutions and formats used for active data, considering performance, cost, and accessibility."
  },
  "Retention": {
    "Description": "Specifies the duration for which different types of data must be kept before deletion or archiving, based on legal, regulatory, and business requirements.",
    "Policies": [
      {
        "DataType": "Customer Records",
        "RetentionPeriod": "7 years",
        "Justification": "Compliance with financial regulations"
      },
      {
        "DataType": "Operational Logs",
        "RetentionPeriod": "6 months",
        "Justification": "Operational efficiency and troubleshooting"
      }
    ]
  },
  "Archiving": {
    "Description": "Describes the process for transferring data from active storage to a less accessible, long-term archival solution. This process should consider data format, access frequency, and retrieval needs.",
    "Strategies": [
      {
        "DataType": "Annual Reports",
        "ArchiveLocation": "Cloud-based long-term storage",
        "AccessRequirements": "Retrievable within 24 hours upon request"
      }
    ]
  },
  "Deletion": {
    "Description": "Defines the protocols for securely and permanently removing data from all storage mediums, ensuring that data is irrecoverable once the retention period has lapsed.",
    "Methods": [
      {
        "DataType": "Temporary Files",
        "DeletionMethod": "Secure Erasure",
        "Validation": "Deletion confirmation logs"
      }
    ]
  }
}
Purpose: Outlines policies and mechanisms for managing the entire lifecycle of data, from creation and storage to archiving and deletion. This includes specifying data retention periods and archiving strategies to comply with regulatory requirements and business needs. DLM ensures efficient and ethical management of data throughout its existence within the platform.
Application: Enables organizations to maintain optimal performance of data systems, comply with legal and regulatory requirements, and ensure that data is accessible and usable over time while also being securely and properly disposed of when no longer needed.
See Example 07.

##### Data Governance (DG)
DG: {...} 
    Purpose: To establish a framework within which data is managed across its entire lifecycle, ensuring its quality, accessibility, consistency, and protection. DG is proactive, focusing on how data supports business objectives, decision-making, and innovation while managing risks.
    Focus: DG encompasses a wider set of internal policies, procedures, standards, and controls that an organization puts in place for effective management of its data assets. This includes data quality, data lifecycle management, data access and control policies, data ownership, and ethical use of data.
    Application: DG is both proactive and reactive, evolving with the organization's strategy, technology landscape, and business needs. It involves cross-functional teams and encompasses a broader scope than compliance alone. 
    Note: While CR ensures that an organization meets external legal and regulatory requirements, DG provides a comprehensive framework for managing data internally, ensuring that it is used and maintained effectively, responsibly, and ethically. DG policies might include standards for data quality, procedures for data sharing and usage within the organization, roles and responsibilities related to data, and internal controls over data management practices.
    Example:
    ```Data Story
    PCS: "Enterprise Data Access Policy" {
      Type: "Policy",
      Version: "1.0.0",
      ...
      DG: {
        DataOwnership: {
          "Sales Data": "Sales Department",
          "Customer Data": "Marketing Department",
          "Employee Data": "Human Resources"
        },
        DataQualityManagement: {
          ResponsibleParty: "Data Governance Team",
          Procedures: "Regular audits, quality checks, and validation processes"
        },
        DataAccessPolicies: {
          "Sales Data": {
            AccessLevel: "Restricted",
            EligibleRoles: ["Sales Manager", "Data Analyst"]
          },
          "Employee Data": {
            AccessLevel: "Confidential",
            EligibleRoles: ["HR Manager", "HR Assistant", "IT Support"]
          }
        },
        EnforcementMechanisms: "Automated access control systems, regular compliance audits"
      },
      ...
    }
    ```

##### Documentation, Metadata, and Data Provenance
This section encompasses comprehensive details on documentation practices, metadata management, data lineage, and data provenance, providing a centralized framework for all aspects related to documenting and understanding data within the Enterprise Data Platform (EDP).

###### Documentation and Metadata (DM)
Purpose: To establish standards and practices for documenting the data, its workflows, and the architecture of the EDP, including the management of metadata, data catalogs, data dictionaries, and business glossaries.
Application: Facilitates understanding, maintenance, and scalability of the platform and its data products by providing clear and accessible documentation and metadata.
Data Cataloging: Logging the metadata of incoming data streams, including source, schema, and version, to aid in data discovery and governance.
Change Tracking: Maintaining logs of schema or data changes over time, supporting audit trails and impact analysis.

###### Data Lineage (DL)
Purpose: To explicitly document the path that data takes through the EDP, from its source to its final form, including all transformations and intermediate stages, aiding in troubleshooting, impact analysis, and understanding data flows.

Application: Details data sources, transformation logic, data stores it moves through, and any changes in format or structure, providing a complete picture of data movement and evolution.
Focus: Data Lineage is a specialized subset of documentation and metadata (DM) with a specialised focus on tracing the life cycle of data, tracking its flow, transformations, and intermediate states from source to destination. It is a dynamic view of the metadata, emphasizing the process and pathways through which data evolves within an EDP. A dedicated DL construct provides a focused, detailed view of the data's journey, essential for impact analysis, audit trails, and troubleshooting data-related issues. It addresses "how" data arrives at its current state.
Example:
```PCS: "Sales Data Analysis Pipeline" {
  Type: "Scenario",
  Version: "2.0.0",
  ...
  DL: {
    "Step1": {
      Description: "Data Extraction from Sales System",
      Source: "CRM System",
      Transformation: "None",
      Output: "Raw Sales Data"
    },
    "Step2": {
      Description: "Data Cleaning and Validation",
      Source: "Raw Sales Data",
      Transformation: "Null value removal, data type validation",
      Output: "Cleaned Sales Data"
    },
    "Step3": {
      Description: "Data Aggregation",
      Source: "Cleaned Sales Data",
      Transformation: "Aggregation by product and region",
      Output: "Aggregated Sales Data for Analysis"
    },
    "Final": {
      Description: "Analysis and Reporting",
      Source: "Aggregated Sales Data for Analysis",
      Transformation: "Analysis for trend identification, report generation",
      Output: "Sales Trend Report"
    }
  },
  ...
}
```

###### Data Provenance (DM-DP)

Purpose: To capture the history of the data, including its original source(s), any assumptions made about the data, and the context in which the data was collected. This is crucial for assessing data quality, reliability, and for making informed decisions based on the data.
Application: Outlines the origin, context, and any external factors affecting the data, such as collection methods, data provider information, and assumptions or transformations applied by the data provider.
Focus: Data Provenance is a specialized subset of documentation and metadata (DM) with a specialised focus on capturing the origin, history, and context of data, providing details on the source, collection methods, assumptions made during collection, and external factors affecting the data. It gives a static view of the data's background and conditions under which it was collected.  It answers "why" and "under what conditions" the data was collected, which is crucial for evaluating its reliability, quality, and applicability for specific analyses.
Example:
PCS: "Customer Feedback Analysis" {
  Type: "Scenario",
  Version: "1.0.0",
  ...
  DP: {
    Source: "Online Feedback Form",
    CollectionMethod: "Automated form submission",
    Assumptions: "Feedback is voluntary, may not represent all customer opinions",
    Context: "Collected from website and mobile app, includes timestamp, customer ID, and feedback text",
    ExternalFactors: "Feedback peak observed post-campaign launches"
  },
  ...
}

##### Interoperability Standards (IS)
IS: {...} 
Purpose: Ensures that the EDP can operate compatibly with other systems, data formats, and technologies. It defines the standards, protocols, and interfaces that support interoperability and seamless data exchange.
Application: Essential for integrating the EDP into a broader IT ecosystem and enabling cross-platform data workflows.

##### Data Transformation and Mapping (DTM)
DTM: "Name" {
  Description: "Detailed description of the data transformation and mapping process, including objectives and expected outcomes.",
  Transformations: [
    {
      Step: "Step Name",
      Description: "Description of the transformation step",
      Rule: "Transformation rule or expression"
    },
    ...
  ],
  Mappings: [
    {
      Source: "Source data element",
      Target: "Target data element",
      MappingRule: "Description or expression for mapping"
    },
    ...
  ],
  Enrichments: [
    {
      Source: "Source data element",
      Enrichment: "Description of the enrichment process",
      DataSources: ["Additional data sources used for enrichment"]
    },
    ...
  ]
}
Purpose: The DTM construct serves to explicitly specify the transformation rules, mapping definitions, and data enrichment processes that occur within the EDP. It encapsulates the logic for converting data from its raw or source format into formats suitable for analysis, reporting, and other business needs. 
Note: this is at a declarative and higher level than might be found in lower-level ETL tools. 

# Example Data Stories

## Example 03
PCS: "Ingest IoT Data for Real-Time Traffic Management" {
  Type: "Scenario",
  Version: "1.0.0",
  Attribution: {
    "Name": "Jane Smith",
    "Affiliation": "Data Story Contributors",
    "Contact": "jane.smith@example.org"
  },
  Status: "Approved",
  DESC: 
  """
  Objective

  To collect and process real-time traffic data from urban sensors, normalizing the data for immediate use in the smart city's traffic management system. The focus is on rapid processing to facilitate real-time traffic adjustments.

  Preconditions

  -  Urban traffic sensors are deployed and operational.
  -  The traffic management system is configured to receive and process data.

  Steps

    Data Collection: Urban traffic sensors collect real-time data on vehicle count and average speed.
    Data Transmission: The collected data is transmitted to the traffic management system using HTTP.
    Format Validation and Conversion: Upon arrival, the system checks the data format. If the data is not in JSON format, it is converted to JSON.
    Data Normalization: The JSON data is normalized to fit the traffic management system's analysis model.
    Analysis and Action: The normalized data is analyzed by the traffic management system, and traffic adjustments are made as needed.

  Alternatives

      Malformed Data Handling: If data received is malformed (incorrect format, missing fields), the system logs the error and excludes the data point from processing.
      Processing Delays: In case of processing delays exceeding the 2-second threshold, prioritize critical data points (e.g., high-traffic areas) to maintain system efficiency.

  Postconditions

      The traffic management system has up-to-date, normalized traffic data available for analysis.
      Traffic adjustments are made in real-time based on the latest data, optimizing traffic flow and reducing congestion.

  Related Scenarios

      Traffic Data Analysis: Further analysis of the ingested data to identify long-term trends and areas for infrastructure improvement.
      Sensor Maintenance and Calibration: Regular checks to ensure the accuracy and operational efficiency of urban traffic sensors.
  """,
  TO: "Emphasize rapid processing over granular accuracy to support real-time traffic adjustments.",
  AS: [{"Traffic Analyst":"Monitors traffic data and makes decisions for traffic management adjustments."}, {"Urban Traffic Sensors":"Devices deployed throughout the city that collect traffic-related data."}, {"Traffic Management System":"The central system that receives processed data for analysis and decision-making."}],
  FC: {
    ON: "data arrival" {
      IF: "(format != JSON)" { "Convert to JSON" },
    }
  },
  IP: "[HTTP for data transmission from sensors]",
  DS: {
    "sensorId": "string",
    "timestamp": "datetime",
    "vehicleCount": "integer",
    "averageSpeed": "float"
  },
  SP: "Ensure data is encrypted during transmission and when stored.",
  QoS: "Achieve data processing latency of less than 2 seconds.",
  EHR: "Upon receiving malformed data, log the error and exclude the data point.",
  ML: "Record sensor ID and data receipt timestamp for each record."
}

## Example 04
PCS: "Data Quality Assurance Framework" {
  Type: "Pattern",
  Version: "1.2.0",
  Attribution: {
    "Name": "Dana Knight",
    "Affiliation": "Quality Insights Inc.",
    "Contact": "dana.knight@example.org"
  },
  Status: "Approved",
  DESC: 
  """
    Context: This pattern is crucial in environments where data is ingested from a variety of sources into an Enterprise Data Platform, requiring consistent quality for reliable analytics and decision-making.

    Problem: Ensuring that data from diverse sources meets predefined quality standards can be challenging, especially when aiming to maintain a balance between thoroughness of data checks and the efficiency of data processing workflows.

    Solution: Implement a data quality assurance framework that automatically evaluates incoming data against set quality criteria. This framework flags data that fails to meet these criteria and notifies the responsible data engineers. The solution emphasizes minimal impact on data ingestion latency while ensuring data quality.

    Diagram: (DATA FLOW DIAGRAM: visualization of the data quality assurance process)

    Implementation:

        Define quality criteria for each data source, including validity, completeness, consistency, and accuracy.
        Integrate a quality assurance system that evaluates incoming data against these criteria during the ingestion process.
        Automate notifications for data engineers when data is flagged for quality issues.
        Ensure flagged data is securely stored for review without significantly impacting the overall data processing flow.

    Examples: A healthcare analytics platform implementing this pattern to ensure the accuracy and completeness of patient data collected from various healthcare providers.

    Related Patterns:

        Metadata Management Strategy: Works in conjunction with the Data Quality Assurance Framework to utilize metadata for tracking the quality and lineage of data.
        Automated Data Cleansing: A complementary pattern that describes approaches for automatically correcting identified quality issues where possible.
  """,
  TO: "Balance between comprehensive data checks and processing efficiency.",
  AS: ["Data Engineer", "Quality Assurance System"],
  FC: {
    ON: "data ingestion" {
      IF: "(data does not meet quality criteria)" {
        "Flag data": "Mark the data as requiring review.",
        "Notify engineer": "Send an alert to the data engineer with details of the quality issue."
      },
      ELSE: {
        "Proceed with processing": "If the data meets all quality criteria, continue to the next step in the data pipeline."
      }
    },
    LOOP: "quality criteria check" {
      "For each data record": {
        "Apply quality checks": "Evaluate the record against all predefined quality criteria.",
        IF: "(any criterion not met)" {
          "Flag and notify": "Invoke the flagging and notification process."
        }
      }
    },
    ON: "flagged data handling" {
      "Secure storage": "Ensure flagged data is encrypted and securely stored for further review.",
      "Logging": "Record details of the quality issue and the data record for auditing and troubleshooting purposes."
    }
  },
  DS: {
    "sourceId": "string",
    "recordId": "string",
    "qualityScore": "float"
  },
  SP: "All data flagged for quality issues must be encrypted and stored securely.",
  QoS: "Data quality checks must not add more than a 10% overhead to ingestion latency."
}

## Example 05
PCS: "Streamlining Passenger Experience Through CRM Data Analysis" {
  Type: "Scenario",
  Version: "1.0.0",
  Attribution: {
    "Name": "Alex Johnson",
    "Affiliation": "Sales Insights Team",
    "Contact": "alex.johnson@example.org"
  },
  Status: "Draft",
  DESC: 
  """
    Objective

    To enhance passenger satisfaction by analyzing Customer Relationship Management (CRM) data, aiming to personalize and improve rail service offerings. The expected outcome is an actionable insight report focusing on service optimization.
    
    Actors

        Data Analyst: Analyzes CRM data to extract meaningful insights for service improvement.
        CRM System: Stores detailed passenger information and feedback.
        Data Warehouse: Integrates CRM data with other operational data sources for comprehensive analysis.

    Preconditions

        The CRM system regularly collects and stores passenger feedback, travel patterns, and preferences.
        Data Warehouse is configured to consolidate CRM data with operational metrics.
        Data Analyst is equipped with the necessary tools and permissions to access and analyze the data.

    Alternatives

      Data Quality Issues: If the CRM data does not meet quality criteria, the Data Analyst flags the issue and notifies the CRM team to rectify the discrepancies before proceeding with the analysis.
      Urgent Findings: For critical insights that demand immediate action (e.g., safety concerns, significant dissatisfaction in a service area), the Data Analyst escalates the findings for rapid response.

      Postconditions

          The rail service has a detailed report of actionable insights into passenger preferences and feedback, enabling targeted improvements in service offerings.
          The Data Analyst has scheduled the next cycle of CRM data analysis, ensuring continuous monitoring and enhancement of passenger experience.

      Related Scenarios

          Feedback Loop Creation: Establishing a mechanism for collecting passenger feedback directly influenced by implemented service changes, ensuring a continuous improvement cycle.
          Service Adjustment Analysis: Analyzing the impact of recent service adjustments on passenger satisfaction and operational efficiency, informing future decisions.
  """
  TO: "Accuracy preferred over speed.",
  AS: ["Data Analyst", "CRM System", "Data Warehouse"],
  FC: {
    LOOP: "Every 24 hours" {
          Data Extraction: Every 24 hours, the CRM system automatically exports passenger data in CSV format over SFTP to the Data Warehouse.
          Data Transformation and Loading: The Data Warehouse processes the CRM data, transforming it to fit the analytical model requirements.
          Analysis: The Data Analyst conducts a thorough analysis of the transformed CRM data, identifying trends in passenger behavior, satisfaction levels, and potential areas for service enhancement.
          Report Generation: Utilizing the insights derived from the analysis, the Data Analyst compiles a comprehensive report outlining recommendations for improving passenger experience.
          Implementation Planning: The findings and recommendations are presented to the rail service management team to plan actionable service improvements.
    }
  },
  IP: "CRM exports data in CSV format over SFTP.",
  DS: {
    "customerId": "string",
    "purchaseAmount": "float",
    "purchaseDate": "datetime"
  },
  SP: "Access to CRM data restricted to authorized roles.",
  QoS: "Transformation process completes within 2 hours of data receipt.",
  EHR: "On error, alert data team via email and attempt retry in 1 hour.",
  ML: "Monitor transformation success rate and processing time."
}

## Example 06
PCS: "Streamlining Data Integration for Retail Analysis" {
	Type: "Story"
	Version: "1.0.0"
	Attribution: {
	"Name": "Emily Rivera",
	"Affiliation": "Analytics Department",
	"Contact": "emily.rivera@example.com"
	}
	Status: "Draft"
	DESC: 
  Narrative:
  """
    - As a Business Analyst in the retail sales division,
    - I want a unified data integration and visualization tool,
    - So that I can efficiently generate comprehensive monthly performance reports. These reports need to capture sales trends, customer behavior, and inventory levels across multiple regions without the current time-consuming manual processes and complex SQL queries. This solution should allow for easy querying and integration of data from disparate sources such as our CRM system, sales database, and inventory management system, thereby enhancing accuracy and insightfulness while reducing the potential for errors in our monthly performance reports.
  """
	TO: "Accuracy and completeness of the integrated data are paramount, even if it requires a bit more time for initial setup. However, the monthly report generation process should be streamlined for speed and efficiency."
	AS: ["Business Analyst", "CRM System", "Sales Database", "Inventory Management System", "Data Integration Tool", "Visualization Platform"]
	FC: {
		"Upon initiating the monthly report creation process": {
			"Check for new or updated data across all sources",
			"Automatically integrate and consolidate data into a unified format",
			"Use predefined templates in the visualization platform to generate reports"
		}
	}
	IP: "Data is exported in a standardized format via API from the CRM system, sales database, and inventory management system. The data integration tool supports RESTful APIs and SQL queries for data extraction and transformation."
	DS: {
		"salesData": {
			"region": "string",
			"totalSales": "float",
			"productCategory": "string",
			"date": "datetime"
		},
		"customerData": {
			"customerId": "string",
			"purchaseHistory": 
				[{productId: string, purchaseDate: datetime, amount: float}],
				"region": "string"
		},
		"inventoryData": {
			"productId": "string",
			"stockLevel": "int",
			"reorderThreshold": "int"
		}
	}
	SP: "Access to the integrated data is restricted to authorized personnel within the Analytics Department to ensure confidentiality and compliance with data protection regulations."
	QoS: "The data integration process completes within 3 hours of initiation, and the visualization platform updates dashboards in real-time as data is processed."
	EHR: "In case of discrepancies or errors during data integration, send an immediate notification to the business analyst and log the incident for review."
	ML: "Monitor and log the frequency of data updates, integration process duration, and user access to the visualization platform for audit and optimization purposes."
}

## Example 07

PCS: "Optimizing Customer Transaction Data Management" {
  Type: "Scenario",
  Version: "2.0.0",
  Attribution: {
    "Name": "Samantha Lee",
    "Affiliation": "Data Governance Team",
    "Contact": "samantha.lee@financialco.org"
  },
  Status: "Approved",
  DESC: 
  """
  Objective:
  
  To streamline the management of customer transaction data, ensuring optimal performance, compliance with financial regulations, and enhancement of data-driven decision-making. The scenario covers the complete lifecycle of transaction data, from its initial collection to its eventual archival or deletion.

  Actors:

      Data Steward: Oversees data quality, compliance, and lifecycle management.
      IT Operations: Manages the technical infrastructure and data storage solutions.
      Compliance Officer: Ensures adherence to financial regulations and data protection laws.

  Preconditions:

      Transaction data is collected in real-time from various banking channels, including online banking, ATMs, and branch transactions.
      The financial institution is subject to regulatory requirements mandating the retention of transaction data for a minimum of seven years.
      Data storage is optimized for fast retrieval to support real-time fraud detection and customer service inquiries.

  Postconditions:

      Transaction data is efficiently managed throughout its lifecycle, balancing operational efficiency, regulatory compliance, and data accessibility for analytics.
      The financial institution can demonstrate compliance with data retention policies and respond promptly to data access requests.
  """,
  TO: "Ensure regulatory compliance and data availability while optimizing storage costs.",
  AS: ["Data Steward", "IT Operations", "Compliance Officer"],
  DLM: {
    "Creation": {
      "Description": "Transaction data is captured in real-time, encrypted, and stored in a high-availability database for immediate processing."
    },
    "Storage": {
      "Description": "After initial processing, data is moved to a secure, cost-efficient storage solution for active use."
    },
    "Retention": {
      "Description": "Transaction data is retained for seven years to comply with financial regulations, with periodic reviews to confirm ongoing relevance.",
      "Policies": [
        {
          "DataType": "Customer Transactions",
          "RetentionPeriod": "7 years",
          "Justification": "Compliance with financial regulations"
        }
      ]
    },
    "Archiving": {
      "Description": "At the end of the retention period, data is archived to cloud-based long-term storage, ensuring data integrity and accessibility for up to ten years.",
      "Strategies": [
        {
          "DataType": "Archived Transactions",
          "ArchiveLocation": "Cloud-based long-term storage",
          "AccessRequirements": "Retrievable within 48 hours upon request"
        }
      ]
    },
    "Deletion": {
      "Description": "Following the archival period, data is securely deleted in compliance with data protection laws, ensuring it is irrecoverable.",
      "Methods": [
        {
          "DataType": "Expired Transaction Archives",
          "DeletionMethod": "Secure Erasure",
          "Validation": "Confirmation of deletion and compliance audit trail"
        }
      ]
    }
  },
  SP: "All transaction data is encrypted in transit and at rest, with access limited to authorized personnel based on their role.",
  QoS: "Transaction processing and fraud detection queries complete within milliseconds; data retrieval for customer service inquiries does not exceed 5 seconds.",
  ML: "System monitors transaction volumes, query performance, and compliance with data management policies, alerting relevant stakeholders to any issues."
}

The Data Story specification provides a structured and expressive framework for detailing the multifaceted requirements of EDPs. With its clear syntax and comprehensive constructs, Data Story is poised to improve the clarity, efficiency, and alignment of data platform projects, fostering a shared understanding and collaboration across diverse stakeholder groups.